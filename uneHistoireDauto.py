import csv
import os
import argparse
import sys
from datetime import datetime
from fonctions.decoupage_liste import decoupage_liste
from fonctions.regroupement_listes import regroupement_listes
from fonctions.tri_liste import tri_liste

csv_entree = 'csv/auto.csv'
csv_sortie = 'csv/sortie.csv' # chemins des fichiers csv
path = 'csv/'

# vérifie la présence du fichier d'entree, sinon le crée
if not os.path.exists(csv_entree):
    print("Le fichier d'entree est introuvable")
    with open(csv_entree, 'w+') as fichierEntree:
        print("fichier_cree:" + str(csv_entree))

# vérifie la présence du fichier de sortie, sinon le crée
if not os.path.exists(csv_sortie):
    print("Le fichier de sortie est introuvable")
    with open(csv_sortie, 'w+') as fichierSortie:
        print("fichier_cree:" + str(csv_sortie))

# valeurs voulues pour les nouvelles colonnes
nouveau_nom_des_colonnes = {
    0:'adresse_titulaire',
    10:'nom',
    7:'prenom',
    8:'immatriculation',
    18:'date_immatriculation',
    14:'vin',
    9:'marque',
    5:'denomination_commerciale',
    3:'couleur',
    1:'carrosserie',
    2:'categorie',
    4:'cylindree',
    6:'energie',
    11:'places',
    12:'poids',
    13:'puissance',
    15:'type',
    16:'variante',
    17:'version'
}

with open(csv_entree) as fichierEntree, open(csv_sortie, 'w+', newline = '') as fichierSortie:
    # écrit dans le fichier de sortie avec le délimiteur ';'
    ecriture = csv.writer(fichierSortie, delimiter = ';')
    # écrit les noms des colonnes précedemment cités
    ecriture.writerow(nouveau_nom_des_colonnes.values())
    # boucle dans le fichier d'entrée les listes à partir de la ligne 1
    for liste_de_base in list(csv.reader(fichierEntree, delimiter = '|'))[1:]:
        # crée une variable reconnaissant la date avec le format suivant
        date_actuelle = datetime.strptime(list(liste_de_base)[5],'%Y-%m-%d')
        # modifie la ligne en regroupant les listes et changeant la date au format souhaité
        liste_triee_par_ligne = regroupement_listes(decoupage_liste(liste_de_base, 15, ", "), [15,5], liste_de_base, date_actuelle.strftime('%d-%m-%Y'))
        # écrit en triant le fichier de sortie
        ecriture.writerow(tri_liste(liste_triee_par_ligne, nouveau_nom_des_colonnes.keys()))