import unittest
from fonctions.decoupage_liste import decoupage_liste
from fonctions.regroupement_listes import regroupement_listes
from fonctions.tri_liste import tri_liste

class testAuto(unittest.TestCase):
    # test du découpage
    def test_decoupage(self):
        test_mot = ["mot_espace_de_tirets_du_8"]
        self.assertEqual(decoupage_liste(test_mot, 0, '_'), ["mot", "espace", "de", "tirets", "du", "8"])
    # test du regroupement
    def test_regroupement(self):
        liste_de_base = ["chaine a garder 1", "chaine a enlever 2"]
        liste_nouvelle =  ["chaine a ajouter 2"]
        date = "31-10-2019"
        self.assertEqual(regroupement_listes(liste_de_base, [1], liste_nouvelle, date), ["chaine a garder 1", "chaine a ajouter 2", "31-10-2019"])
    # test du tri
    def test_tri(self):
        liste_entree = ["chaine 1", "chaine 2", "chaine 3"]
        dictionnaire_entree = {
            1: "chaine 2",
            2: "chaine 1",
            3: "chaine 3"
        }
        self.assertEqual(tri_liste(liste_entree, dictionnaire_entree.keys()), ["chaine 2", "chaine 1", "chaine 3"])
    
